<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'PlannerController@index')->name('home');
Route::post('/addCourse', 'PlannerController@addCourse');
Route::post('/removeCourse', 'PlannerController@removeCourse');
Route::post('/addSemester', 'PlannerController@addSemester');
Route::post('/save', 'PlannerController@saveCurriculum');

Route::resource('/curriculum', 'CurriculumController');