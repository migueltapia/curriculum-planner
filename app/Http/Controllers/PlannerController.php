<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App;
use DB;

class PlannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // All saved curriculums
        $saved = App\Curriculum::where('name', '!=', 'unsaved')->get();

        // check for current 'unsaved'
        $curriculum = App\Curriculum::where('name', 'unsaved')->first();

        // if no 'unsaved' then create new one
        if (empty($curriculum)) {
            $curriculum = App\Curriculum::create(['name' => 'unsaved']);
            $curriculum->semester()->create([
                'title' => 'Taken',
                'order' => '1'
            ]);
        }

        // get available courses for each semester and compile into array
        $available_courses = $this->availableCourses($curriculum->id);

        return view('planner', [
            'init_available_courses' => $available_courses,
            'curricula'             => $saved,
            'current_curriculum'    => $curriculum
        ]);
    }

    /**
     * Store added Course.
     *
     * @param  \Illuminate\Http\Request  $request(curriculum_id, order, course_id)
     * @return \Illuminate\Http\Response 
     */
    public function addCourse(Request $request)
    {
        // Retrive semester model from current curriculum
        $semester = App\Semester::byCurriculumAndOrder($request->curriculum_id, $request->order)->firstOrFail();

        // add course to semester model
        $semester->course()->attach($request->course_id);

        // return new list of available courses
        return $this->updatedData($request->curriculum_id);
    }

    /**
     * Remove Course.
     *
     * @param  \Illuminate\Http\Request  $request(curriculum_id, order, course_id)
     * @return \Illuminate\Http\Response 
     */
    public function removeCourse(Request $request)
    {
        // Check if able to remove, not pre/corequired by another course



        
        // Retrive semester model from current curriculum
        $semester = App\Semester::byCurriculumAndOrder($request->curriculum_id, $request->order)->firstOrFail();

        // remove course from semester model
        $semester->course()->detach($request->course_id);

        // return new list of available courses
        return $this->updatedData($request->curriculum_id);
    }

    /**
     * Store added Sememster.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function addSemester(Request $request)
    {
        $semester = new App\Semester;

        $semester->title = $request->title;
        $semester->order = $request->order;
        $semester->curriculum()->associate($request->curriculum_id);

        $semester->save();

        // return new list of available courses
        return $this->updatedData($request->curriculum_id);
    }

    /**
     * Store current curriculum.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function saveCurriculum(Request $request)
    {
        
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $curriculum_id
     * @param  int  $semester_order
     * @return \Illuminate\Database\Eloquent\Collection $courses
     */
    public function availableCourses($curriculum_id)
    {
        $courses = []; 
        $semesters = App\Semester::where('curriculum_id', $curriculum_id)->get();
        $all_curriculum_courses = App\Course::whereHas('semester', function ($query) use (&$curriculum_id) {
            $query->where('curriculum_id', $curriculum_id);
        })->pluck('id');
        foreach ($semesters as $semester) {
            $semester_order = $semester->order;

            // pre
            // need all courses from previous semesters
            $previous_semester_courses = App\Course::whereHas('semester', function ($query) use (&$curriculum_id, &$semester_order) {
                $query->where([
                    ['curriculum_id', $curriculum_id],
                    ['order', '<', $semester_order],
                ]);
            })->get();

            // co
            // need all courses from current and previous semesters
            $previous_and_current_semester_courses = App\Course::whereHas('semester', function ($query) use (&$curriculum_id, &$semester_order) {
                $query->where([
                    ['curriculum_id', $curriculum_id],
                    ['order', '<=', $semester_order],
                ]);
            })->get();

            $semester_courses = collect();
            foreach (App\Course::whereNotIn('id', $all_curriculum_courses)->with('prerequisite', 'corequisite')->cursor() as $course) {
                // pre set requisete flags in case one is not needed
                $prerequisite_met = true;
                $corequisite_met = true;

                // loop check all pre requisets
                foreach ($course->prerequisite as $prerequisite) {
                    //check for pre
                    if (!$previous_semester_courses->contains('id', $prerequisite->id)) {
                        $prerequisite_met = false;
                        break;
                    }
                }
                // loop check all co requisets
                foreach ($course->corequisite as $corequisite) {
                    //check for co
                    if (!$previous_and_current_semester_courses->contains('id', $corequisite->id)) {
                        $corequisite_met = false;
                        break;
                    }
                }

                // add to collection if both pass
                if ($prerequisite_met && $corequisite_met) {
                    $semester_courses->push($course);
                }
            }
            array_push($courses, $semester_courses);
        }
        return $courses;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $curriculum_id
     * @return array $new_data
     */
    public function updatedData($curriculum_id)
    {
        $curriculum = App\Curriculum::find($curriculum_id)->semester->load('course')->toArray();
        $new_data = [ 
            'courses' => $this->availableCourses($curriculum_id), 
            'semesters' => $curriculum
        ];
        return $new_data;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updated(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
