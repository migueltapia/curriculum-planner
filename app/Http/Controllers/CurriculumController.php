<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App;
use App\Http\Controllers\PlannerController;

class CurriculumController extends Controller
{
    /**
     * Using for reset of curriculum
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // find unaved 
        $unsaved = App\Curriculum::where('name', 'unsaved')->first();
        // remove if found
        if (!empty($unsaved)) {
            $this->destroy($unsaved->id);
        }
        
        // redirect back to home wich will create new unsaved
        return route('home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $curriculum = App\Curriculum::find($request->curriculum_id);

        $curriculum->name = $request->name;

        $curriculum->save();

        return route('curriculum.show', [$curriculum]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, PlannerController $planner)
    {
        // All saved curriculums
        $saved = App\Curriculum::where('name', '!=', 'unsaved')->get();

        // check for current 'unsaved'
        $curriculum = App\Curriculum::find($id);

        // get available courses for each semester and compile into array
        $available_courses = $planner->availableCourses($curriculum->id);

        return view('planner', [
            'init_available_courses' => $available_courses,
            'curricula'             => $saved,
            'current_curriculum'    => $curriculum
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $curriculum = App\Curriculum::findorFail($id);
        $curriculum->delete();
    }
}
