<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Semester extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'order'];

    /**
     * Get the curriculum that owns the semester.
     */
    public function curriculum()
    {
        return $this->belongsTo('App\Curriculum');
    }

    /**
     * Get the course/classs for the semester.
     */
    public function course()
    {
        return $this->belongsToMany('App\Course');
    }

    /**
     * Scope a query to only include semester of specified curriculum and order.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param  int  $curriculum_id
     * @param  int  $semester_order
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeByCurriculumAndOrder($query, $curriculum_id, $semester_order)
    {
        return $query->where([
            ['curriculum_id', '=', $curriculum_id],
            ['order', '=', $semester_order]
        ]);
    }
}
