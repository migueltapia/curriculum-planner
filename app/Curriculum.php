<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Curriculum extends Model
{
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * Get the semester for the curriculum.
     */
    public function semester()
    {
        return $this->hasMany('App\Semester');
    }

    /**
     * Get all of the courses for the curriculum.
     */
    public function courses()
    {
        return $this->hasManyThrough('App\Course', 'App\Semester');
    }
}
