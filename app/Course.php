<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    /**
     * Get the prerequired for the class.
     */
    public function prerequisite()
    {
        return $this->belongsToMany('App\Course', 'prerequisites', 'courses_id', 'prerequisite_id');
    }
    /**
     * Get the classes for the prerequisite.
     */
    public function prerequisiteCourse()
    {
        return $this->belongsToMany('App\Course', 'prerequisites', 'prerequisite_id', 'courses_id');
    }

    /**
     * Get the corequired for the class.
     */
    public function corequisite()
    {
        return $this->belongsToMany('App\Course', 'corequisites', 'courses_id', 'corequisite_id');
    }

    /**
     * Get the classes for the corequisite.
     */
    public function corequisiteCourse()
    {
        return $this->belongsToMany('App\Course', 'corequisites', 'corequisite_id', 'courses_id');
    }

    /**
     * Get the semester for the class.
     */
    public function semester()
    {
        return $this->belongsToMany('App\Semester');
    }
}
