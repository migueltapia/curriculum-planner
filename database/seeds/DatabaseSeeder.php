<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(Fall2016_Spring2017_Undergraduate_Resident_EN_Tuition::class);
        $this->call(MEEN_Curriculum::class);
    }
}
