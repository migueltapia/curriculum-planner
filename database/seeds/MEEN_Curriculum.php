<?php

use Illuminate\Database\Seeder;
use App\Course;

class MEEN_Curriculum extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $classes =  [
        	'1'		=>	[
            	'name'		=>	'ENGR 111',
            	'title'		=>	'Foundations of Engr. I',
            	'hrs'		=>	'2'
                ],
        	'2'		=>	[
            	'name'		=>	'MATH 151',
            	'title'		=>	'Engineering Math I',
            	'hrs'		=>	'4'
        	    ],
        	'3'		=>	[
            	'name'		=>	'PHYS 218',
            	'title'		=>	'Mechanics',
            	'hrs'		=>	'4'
        	    ],
        	'4'		=>	[
            	'name'		=>	'ENGL 104',
            	'title'		=>	'Composition & Rhetoric',
            	'hrs'		=>	'3'
        	    ],
        	'5'		=>	[
            	'name'		=>	'UCC 1',
            	'title'		=>	'Elective 1',
            	'hrs'		=>	'3'
        	    ],


        	'6'		=>	[
            	'name'		=>	'ENGR 112',
            	'title'		=>	'Foundations of Engr. II',
            	'hrs'		=>	'2'
        	    ],
        	'7'		=>	[
            	'name'		=>	'MATH 152',
            	'title'		=>	'Engineering Math II',
            	'hrs'		=>	'4'
        	    ],
        	'8'		=>	[
            	'name'		=>	'PHYS 208',
            	'title'		=>	'Electricity & Optics',
            	'hrs'		=>	'4'
        	    ],
        	'9'		=>	[
            	'name'		=>	'CHEM 107/117',
            	'title'		=>	'Gen. Chem. for Engineers/Lab',
            	'hrs'		=>	'4'
        	    ],
        	'10'         =>  [
            	'name'		=>	'UCC 2',
            	'title'		=>	'Elective 2',
            	'hrs'		=>	'3'
        	   ],


        	'11'         =>  [
            	'name'		=>	'MATH 251',
            	'title'		=>	'Engineering Math III',
            	'hrs'		=>	'3'
        	   ],
        	'12'         =>  [
            	'name'		=>	'MEEN 225',
            	'title'		=>	'Engineering Mechanics',
            	'hrs'		=>	'3'
        	   ],
        	'13'         =>  [
            	'name'		=>	'MEEN 222',
            	'title'		=>	'Materials Science',
            	'hrs'		=>	'3'
        	   ],
        	'14'         =>  [
            	'name'		=>	'MEEN 210',
            	'title'		=>	'Modeling for Mech. Design',
            	'hrs'		=>	'2'
        	   ],
        	'15'         =>  [
            	'name'		=>	'UCC 3',
            	'title'		=>	'Elective 3',
            	'hrs'		=>	'3'
        	   ],
        	'16'         =>  [
            	'name'		=>	'COMM 205/ENGL 203or210',
            	'title'		=>	'Communication Elective',
            	'hrs'		=>	'3'
        	   ],


        	'17'         =>  [
            	'name'		=>	'MATH 308',
            	'title'		=>	'Differenial Equations',
            	'hrs'		=>	'3'
               ],
        	'18'         =>  [
            	'name'		=>	'ECEN 215',
            	'title'		=>	'Prin. of Electrical Engr.',
            	'hrs'		=>	'3'
        	   ],
        	'19'         =>  [
            	'name'		=>	'CVEN 305',
            	'title'		=>	'Mechanincs of Materials',
            	'hrs'		=>	'3'
               ],
        	'20'         =>  [
            	'name'		=>	'MEEN 315',
            	'title'		=>	'Prin. of Thermodynamics',
            	'hrs'		=>	'3'
        	   ],
        	'21'         =>  [
            	'name'		=>	'MEEN 260',
            	'title'		=>	'Mechanical Measurements',
            	'hrs'		=>	'3'
               ],


            '22'        =>  [
            	'name'		=>	'MEEN 357',
            	'title'		=>	'Engr. Analyssi for Mech.',
            	'hrs'		=>	'3'
               ],
            '23'        =>  [
            	'name'		=>	'MEEN 363',
            	'title'		=>	'Dynamics and Vibrations',
            	'hrs'		=>	'3'
               ],
            '24'        =>  [
            	'name'		=>	'MEEN 360',
            	'title'		=>	'Mtls. & Mfg. Select. in Dsn.',
            	'hrs'		=>	'3'
               ],
            '25'        =>  [
            	'name'		=>	'MEEN 361',
            	'title'		=>	'Mtls. & Mfg. Lab',
            	'hrs'		=>	'1'
               ],
            '26'        =>  [
            	'name'		=>	'MEEN 344',
            	'title'		=>	'Flui Mechanics',
            	'hrs'		=>	'3'
               ],
            '27'        =>  [
            	'name'		=>	'MEEN 345',
            	'title'		=>	'Fluid Mechanics Lab',
            	'hrs'		=>	'1'
               ],
            '28'        =>  [
            	'name'		=>	'UCC ICD 1',
            	'title'		=>	'Elective 1',
            	'hrs'		=>	'3'
               ],


            '29'        =>  [
            	'name'		=>	'MEEN 368',
            	'title'		=>	'Solid Mech. in Mech. Design',
            	'hrs'		=>	'3'
               ],
            '30'        =>  [
            	'name'		=>	'MEEN 461',
            	'title'		=>	'Heat Transfer',
            	'hrs'		=>	'3'
               ],
            '31'        =>  [
            	'name'		=>	'MEEN 464',
            	'title'		=>	'Heat Transfer Lab',
            	'hrs'		=>	'1'
               ],
            '32'        =>  [
            	'name'		=>	'MEEN 364',
            	'title'		=>	'Dynamic Syst. and Control',
            	'hrs'		=>	'3'
               ],
            '33'        =>  [
            	'name'		=>	'ISEN 302',
            	'title'		=>	'Econ. Anly. Engr. Project',
            	'hrs'		=>	'2'
               ],
            '34'        =>  [
            	'name'		=>	'MEEN 381',
            	'title'		=>	'Seminar',
            	'hrs'		=>	'1'
               ],
            '35'        =>  [
            	'name'		=>	'UCC ICD 2',
            	'title'		=>	'Elective 2',
            	'hrs'		=>	'3'
               ],


            '36'        =>  [
            	'name'		=>	'MEEN 401',
            	'title'		=>	'Intro. to Mech. Engr. Design',
            	'hrs'		=>	'3'
               ],
            '37'        =>  [
            	'name'		=>	'MEEN 404',
            	'title'		=>	'Engineering Laboratory',
            	'hrs'		=>	'3'
               ],
            '38'        =>  [
            	'name'		=>	'MEEN Stem 1',
            	'title'		=>	'MEEN 421, 431, or 475',
            	'hrs'		=>	'3'
               ],
            '39'        =>  [
            	'name'		=>	'MEEN/Non-MEEN 1',
            	'title'		=>	'Technical Elective',
            	'hrs'		=>	'3'
               ],
            '40'        =>  [
            	'name'		=>	'UCC 4',
            	'title'		=>	'Elective 4',
            	'hrs'		=>	'3'
               ],


            '41'        =>  [
            	'name'		=>	'MEEN 402',
            	'title'		=>	'Intermediate Design',
            	'hrs'		=>	'3'
               ],
            '42'        =>  [
            	'name'		=>	'MEEN',
            	'title'		=>	'Technical Elective',
            	'hrs'		=>	'3'
               ],
            '43'        =>  [
            	'name'		=>	'MEEN Stem 2',
            	'title'		=>	'MEEN 421, 431, or 475',
            	'hrs'		=>	'3'
               ],
            '44'        =>  [
            	'name'		=>	'MEEN/Non-MEEN 2',
            	'title'		=>	'Technical Elective',   
            	'hrs'		=>	'3'
               ],
            '45'        =>  [
            	'name'		=>	'ENGR/PHIL 482',
            	'title'		=>	'Engineering Ethics',
            	'hrs'		=>	'3'
               ]
        ];

        foreach ($classes as $id => $course) {
            DB::table('courses')->insert([
                'id'         =>  $id,
                'name'       =>  $course['name'],
                'title'      =>  $course['title'],
                'hrs'        =>  $course['hrs']
                ]);           
        }

        // use 'pre' for prerequired courses
        // use 'co' for co-requred courses
        // if none of eithr does not need to be included
		$requisites = [
			'1'	=>	[
				'co' => ['2']
			],
			'3' =>  [
				'co' => ['2']
			],
			'6' =>  [
				'pre' => ['1', '2']
			],
			'7' =>  [
				'pre' => ['2']
			],
			'8' =>  [
				'pre' => ['3'],
				'co' => ['7']
			],


			'11' =>  [
				'pre' => ['7']
			],
			'12' =>  [
				'pre' => ['3'],
				'co' => ['11']
			],
			'13' =>  [
				'pre' => ['3', '9']
			],
			'14' =>  [
				'pre' => ['1']
			],
			'17' =>  [
				'pre' => ['11']
			],
			'18' =>  [
				'pre' => ['8', '6'],
				'co'  => ['17']
			],
			'19' =>  [
				'pre' => ['12'],
			],
			'20' =>  [
				'pre' => ['12', '11'],
			],
			'21' =>  [
				'pre' => ['12'],
				'co'  => ['18', '20', '17']
			],


			'22' =>  [
				'pre' => ['6', '17', '14'],
			],
			'23' =>  [
				'pre' => ['12', '17'],
				'co'  => ['22', '19']
			],
			'24' =>  [
				'pre' => ['13', '21', '19', '14']
			],
			'25' =>  [
				'pre' => ['13', '21', '19'],
				'co'  => ['24']
			],
			'26' =>  [
				'pre' => ['12', '20']
			],
			'27' =>  [
				'pre' => ['21'],
				'co'  => ['26']
			],
			'29' =>  [
				'pre' => ['19'],
				'co'  => ['22', '24']
			],
			'30' =>  [
				'pre' => ['17', '26']
			],
			'31' =>  [
				'pre' => ['27'],
				'co'  => ['30']
			],
            '32' =>  [
                'pre' => ['18', '23', '21']
            ],
			'33' =>  [
				'pre' => ['7']
			],


			'36' =>  [
				'pre' => ['24', '25', '32', '29', '30']
			],
			'37' =>  [
				'pre' => ['24', '25', '32', '30'],
				'co'  => ['36']
			],
			'41' =>  [
				'pre' => ['36']
			],
		];

		foreach ($requisites as $id => $requisite) {
			$course = Course::find($id);
			$course->prerequisite()->sync(isset($requisite['pre']) ? $requisite['pre'] : []);
			$course->corequisite()->sync(isset($requisite['co']) ? $requisite['co'] : []);			
		}

    }
}
