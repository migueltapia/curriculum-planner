<?php

use Illuminate\Database\Seeder;

class Fall2016_Spring2017_Undergraduate_Resident_EN_Tuition extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tuitions')->insert([
			'rate_code'	=>	'TLR_1617 EN',
			'1hrs'	=>	'873.01',
			'2hrs'	=>	'1261.73',
			'3hrs'	=>	'1650.45',
			'4hrs'	=>	'2039.17',
			'5hrs'	=>	'2427.89',
			'6hrs'	=>	'2816.61',
			'7hrs'	=>	'3205.33',
			'8hrs'	=>	'4760.26',
			'9hrs'	=>	'4760.26',
			'10hrs'	=>	'4760.26',
			'11hrs'	=>	'4760.26',
			'12plus_hrs'	=>	'6015.19'
		]);
			
        DB::table('tuitions')->insert([
			'rate_code'	=>	'TVR_VARI EN',
			'1hrs'	=>	'847.5',
			'2hrs'	=>	'1226.40',
			'3hrs'	=>	'1605.30',
			'4hrs'	=>	'1984.20',
			'5hrs'	=>	'2363.10',
			'6hrs'	=>	'2742.00',
			'7hrs'	=>	'3120.90',
			'8hrs'	=>	'4636.59',
			'9hrs'	=>	'4636.59',
			'10hrs'	=>	'4636.59',
			'11hrs'	=>	'4636.59',
			'12plus_hrs'	=>	'5853.87'
		]);
			
        DB::table('tuitions')->insert([
			'rate_code'	=>	'TLR 1516 EN',
			'1hrs'	=>	'814.09',
			'2hrs'	=>	'1169.68',
			'3hrs'	=>	'1525.27',
			'4hrs'	=>	'1880.86',
			'5hrs'	=>	'2236.45',
			'6hrs'	=>	'2592.04',
			'7hrs'	=>	'2947.63',
			'8hrs'	=>	'4369.89',
			'9hrs'	=>	'4369.89',
			'10hrs'	=>	'4369.89',
			'11hrs'	=>	'4369.89',
			'12plus_hrs'	=>	'5538.70',
					
			'rate_code'	=>	'TLR 1415 EN',
			'1hrs'	=>	'796.36',
			'2hrs'	=>	'1142.89',
			'3hrs'	=>	'1489.42',
			'4hrs'	=>	'1835.95',
			'5hrs'	=>	'2182.48',
			'6hrs'	=>	'2529.01',
			'7hrs'	=>	'2875.54',
			'8hrs'	=>	'4261.64',
			'9hrs'	=>	'4261.64',
			'10hrs'	=>	'4261.64',
			'11hrs'	=>	'4261.64',
			'12plus_hrs'	=>	'5399.03'
        ]);
    }
}
