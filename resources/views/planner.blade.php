<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Planner</title>

    <!-- Bootstrap -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles based on 'cover' template -->
    <link href="{{asset('css/custom.css')}}" rel="stylesheet">
  </head>

  <body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header"><a class="navbar-brand" href="/">Course Planner</a></div>
      </div>
    </nav>

    <div class="container-fluid">
      <div class="row" id="planner">

        <!-- List of all classes available to add -->
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <!-- Links will not lead to anywhere, just using for the styles already set up for it -->
            <li class="active"><a href="#">Available Classes To Add <span class="sr-only">(current)</span></a></li>
            <li v-for="course in availableCourses[(availableCourses.length - 1)]">@{{ course.name }} - @{{ course.title }}</li>
          </ul>
        </div>

        <!-- List of saved Curricula -->
        <div class="col-sm-3 col-md-2 sidebar-right">
          <ul class="nav nav-sidebar">
            <li class="active"><a href="#">Previously Saved Curriculums <span class="sr-only">(current)</span></a></li>
          </ul>
          <ul class="nav nav-sidebar">
            <div class="text-center"><button type="button" class="btn btn-default btn-sm"  @click="saveCurriculum">Save current curriculum </button></div>
            @foreach ($curricula as $curriculum)
            <li><a href="{{ route('curriculum.show', ['id' => $curriculum->id]) }}">{{ $curriculum->name }}</a></li>
            @endforeach
          </ul>
        </div>

        <!-- Display of Semesters, Courses and information -->
        <div class="col-sm-6 col-sm-offset-3 col-md-8 col-md-offset-2 main">
          <!-- Dashboard -->
          <div>
            <button type="button" class="btn btn-danger" @click="resetCurriculum">
              Start Over <span class="glyphicon glyphicon-erase" aria-hidden="true"></span>
            </button>
          </div>

          <div class="alert alert-danger" role="alert">
            Please remember that although some courses do not have any set per-required courses needed, they may have other restrictions.  Like those only available during your junior and senior year or those that require approval from an adviser.
          </div>

          <template v-for="semester in semesters">
            <div class="panel panel-warning">
              <div class="panel-heading">
                <div class="row">
                  <div class="col-md-7">
                    <h4>@{{ semester.title }} </h4>
                  </div>
                  <div class="col-md-5 text-right">
                    <span class="label"
                      :class="colorSemesterHrs(semester.order)">@{{ sumSemesterHrs(semester.order) }} Hrs</span></div>
                </div>
              </div>
              <div class="panel-body">

                <!-- Added Courses -->
                <div class="btn-group" v-for="courses in semester.course">
                  <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    @{{courses.name}}<span class="caret"></span>
                  </button>
                  <ul class="dropdown-menu">
                    <li><a @click="removeCourse(courses.id, semester.order)">Remove</a></li>
                  </ul>
                </div>

                <!-- Add course button -->
                <div class="btn-group">
                  <button class="btn btn-warning btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Add Course <span class="caret"></span>
                  </button>
                  <ul class="dropdown-menu">
                    <li v-for="course in availableCourses[(semester.order - 1)]">
                      <a @click="addCourse(course.id, semester.order)">
                      @{{ course.name }}
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </template>

          <button type="button" class="btn btn-warning btn-lg" @click="addSemester">
              Add New Semester <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
          </button>

        </div>
      </div>
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <!-- Vue.js source -->
    <script src="https://unpkg.com/vue/dist/vue.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/1.0.3/vue-resource.js"></script>

    <!-- Vue.js code -->
    <script type="text/javascript">
      var vm = new Vue({
        el: '#planner',
        data: {
          semesters: {!! $current_curriculum->semester->load('course') !!},
          availableCourses:  {!! json_encode($init_available_courses) !!}
        },
        methods: {
          addCourse: function (course, semester_order) {
            this.$http.post('/addCourse', {
              order: semester_order,
              curriculum_id: {{ $current_curriculum->id }},
              course_id: course
            }).then((response) => {
              // success callback
              this.updateData(response);
            }, (response) => {
              // error callback
            });
          },
          removeCourse: function (course, semester_order) {
            this.$http.post('/removeCourse', {
              order: semester_order,
              curriculum_id: {{ $current_curriculum->id }},
              course_id: course
            }).then((response) => {
              // success callback
              this.updateData(response);
            }, (response) => {
              // error callback
            });
          },
          addSemester: function () {
            this.$http.post('/addSemester', {
              title: 'Semester '+(this.semesters.length+1), 
              order: this.semesters.length+1,
              curriculum_id: {{ $current_curriculum->id }}
            }).then((response) => {
              // success callback
              this.updateData(response);
            }, (response) => {
              // error callback
            });
          },
          sumSemesterHrs: function (semester_order) {
            hrs = 0
            for (course of this.semesters[(semester_order - 1)].course) {
              hrs += course.hrs
            }
            return hrs
          },
          colorSemesterHrs: function (semester_order) {
            if (semester_order == 1) { return 'label-default' }
            hrs = this.sumSemesterHrs(semester_order);
            switch (hrs) {
                default:
                  return 'label-primary'
            }
          },
          {{-- POST will not redirect --}}
          saveCurriculum: function () {
            var curriculum_name = prompt('Type name for this curriculum.');
            if (curriculum_name !== 'unsaved') {
              this.$http.post('/curriculum', {
                name: curriculum_name,
                curriculum_id: {{ $current_curriculum->id }}
              }).then((response) => {
                // success callback
                window.location = response.data;
              }, (response) => {
                // error callback
              }); 
            } else {    
              alert('Cannon save curriculum as "unsaved". Try saving again with another name.') 
            };         
          },
          resetCurriculum: function () {
            this.$http.get('/curriculum').then((response) => {
              // success callback
              window.location = response.data;
            }, (response) => {
              // error callback
            });            
          },
          viewCurriculum: function ($curriculum_id) {
            this.$http.get('/curriculum/' + $curriculum_id).then((response) => {
              // success callbac
              //window.location = response;
            }, (response) => {
              // error callback
            }); 
          },
          updateData: function (response) {
              this.availableCourses = response.data.courses
              this.semesters = response.data.semesters
          }
        }
      })
    </script>
  </body>
</html>
